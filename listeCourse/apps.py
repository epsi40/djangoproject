from django.apps import AppConfig


class ListecourseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'listeCourse'
