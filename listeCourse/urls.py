from django.contrib import admin
from django.urls import path, include
from listeCourse import views

urlpatterns = [
    path('getStore/<str:store_name>',views.get_store),
    path('store/',views.all_stores)
]