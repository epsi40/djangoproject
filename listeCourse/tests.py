from django.test import TestCase
from .models import Store

# Create your tests here.

class testStore(TestCase):
    def setup(self):
        Store.objects.create(name="storeTest")

    def test_one_Store(self):
        result = Store.objects.all()
        self.assertEqual(1, len(result))
        self.assertEqual(result[0].name, "storeTest")
