from django.shortcuts import render
from django.http import HttpResponse
from .models import Store

# Create your views here.
def index(request):
    return HttpResponse('Index of shared list'+request)

def all_stores(request):
    stores = Store.objects.all()
    return HttpResponse(stores)

def create_store(request, store_name):
    store = Store(store_name)
    store.save()
    return HttpResponse(store)

def get_store(request):
    stores = Store.objects.all()
    return HttpResponse(stores)