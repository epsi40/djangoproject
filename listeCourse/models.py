from django.db import models

# Create your models here.
class ListeCourse(models.Model):
    name  = models.CharField(max_length=250)

class Store(models.Model):
    name  = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class ItemInStore(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE)

class Article(models.Model):
    name = models.CharField(max_length=250)
    price = models.CharField(max_length=2)

    def __str__(self):
        return self.name
    
